//
// Created by spokomaciek on 06.01.17.
//

#ifndef SK2_PROJEKT_FILEMANAGER_H
#define SK2_PROJEKT_FILEMANAGER_H


#include <map>
#include <vector>
#include "openfile.h"

class FileManager {
    // wszystkie pliki
    std::vector<std::string> availableFiles;

    // pliki otwarte
    std::map<std::string, int> openFiles;
    std::map<int, OpenFile*> Files;

    int last_id;
    pthread_mutex_t adder;
public:
    FileManager();
    OpenFile* getFile(std::string name);
    int getFileId(std::string name);
    std::string getContentFromId(int id);
    void oneMore(int id, int socket);   // dołącznie klienta do pliku
    void oneLess(int id, int socket);   // odłączanie klienta od pliku
    std::string getFileList();          // lista plików w odpowiednim formacie
};


#endif //SK2_PROJEKT_FILEMANAGER_H
