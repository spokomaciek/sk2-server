//
// Created by spokomaciek on 29.12.16.
//

#ifndef SK2_PROJEKT_SERVER_H
#define SK2_PROJEKT_SERVER_H

#include <netinet/in.h>
#include "filemanager.h"
#include "client.h"

#define QUEUE_SIZE 10
#define BUF_SIZE 1024
class Client;

class Server {
private:
    int socket;
    void play();
    void handleConnection(int client_socket, sockaddr_in client_address);

    std::map<pthread_t, Client*> clients;
public:
    Server();
    bool createServer(int port);
    static void* asyncPlay(void *object);
    ~Server();
    FileManager myManager;
    void terminateClient(int socket);
    void KillEmAll();
};


#endif //SK2_PROJEKT_SERVER_H
