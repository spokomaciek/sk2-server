//
// Created by spokomaciek on 06.01.17.
//

#ifndef SK2_PROJEKT_CLIENT_H
#define SK2_PROJEKT_CLIENT_H
#include <string>
#include "queue.h"
#include "server.h"

class Server;


class Client {
public:
    std::string ip;
    int port;
    OpenFile *file;
    int socket;
    int docId;
    Client(std::string ip, int port, int socket, Server *parent);

    // do uruchamiania play() w nowym wątku
    static void *asyncPlay(void *object);
    void play();
private:
    std::string doc;
    Server *parent;
};


#endif //SK2_PROJEKT_CLIENT_H
