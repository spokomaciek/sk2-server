//
// Created by spokomaciek on 06.01.17.
//
#include <cstring>
#include <cstdio>
#include <zconf.h>
#include "client.h"

Client::Client(std::string ip, int port, int socket, Server *parent) : ip(ip), port(port), socket(socket), ver(0), parent(parent) {
    docId = -1;
}

void *Client::asyncPlay(void *object) {
    reinterpret_cast<Client*>(object)->play();
    return 0;
}

// dopisanie zer z lewej strony
std::string leftpad(unsigned long n, unsigned len) {
    std::string temp = std::to_string(n);
    while (temp.size() < len) {
        temp = "0"+temp;
    }
    return temp;
}


// odbieranie wiadomości
void Client::play() {
    char buf[BUF_SIZE];
    int c;

    int offset = 0;
    do {
        memset(buf, '\0', BUF_SIZE);
        c = recv(this->socket, buf + offset, BUF_SIZE - offset, 0);
        printf("received %s on socket %d\n", buf, socket);
        if (c == -1) {
            perror("Odbieranie wiadomości od serwera.");
        }
        else if (c == 0) {
            printf("Klient zamknięty.\n");
            break;
        }
        std::string msg, tmp;
        for (int i = 1; i < c; ++i) {
            msg += buf[i];
        }
        if (buf[0] - '0' == OPEN) {
            if (docId != -1) {
                parent->myManager.oneLess(docId, socket);
                file = nullptr;
                docId = -1;
            }
            std::string fname;
            for (int i = 7; buf[i] != '\n' && i < c; ++i) {
                fname += buf[i];
            }

            file = parent->myManager.getFile(fname);
            docId = parent->myManager.getFileId(fname);
            if (!docId) {
                return;
            }
            doc = parent->myManager.getContentFromId(docId);
            std::string frag = doc.substr(0, 50);
            std::string temp = std::to_string(INSERT) + leftpad(0, 3) + leftpad(frag.size(), 3) + frag + "\n";
            write(socket, temp.c_str(), temp.size());
            printf("wrote %s on socket %d\n", temp.c_str(), socket);
            for (unsigned i = 50; i < doc.size(); i+=50) {
                frag = doc.substr(i, 50);
                temp = std::to_string(INSERT) + leftpad(i, 3) + leftpad(frag.size(), 3) + frag + "\n";
                write(socket, temp.c_str(), temp.size());
                printf("wrote %s on socket %d\n", temp.c_str(), socket);
            }
            write(socket, "00000000\n", 9);
            parent->myManager.oneMore(docId, socket);

        }
        else if (buf[0] - '0' == FILELIST) {
            std::string filelist = parent->myManager.getFileList();
            write(socket, filelist.c_str(), filelist.size());
            printf("wrote %s on socket %d\n", filelist.c_str(), socket);
        }
        if (docId == -1) {
            continue;
        }
        else if (buf[0] - '0' == INSERT) {
            tmp = msg.substr(0,3);
            int pos = atoi(tmp.c_str());
            tmp = msg.substr(3, 3);
            int len = atoi(tmp.c_str());
            tmp = msg.substr(6);
            tmp = tmp.substr(0, tmp.size() - 1);
            file->AddOp(INSERT, pos, len, tmp, socket);
        }
        else if (buf[0] - '0' == DELETE) {
            tmp = msg.substr(0,3);
            int pos = atoi(tmp.c_str());
            tmp = msg.substr(4, 3);
            int len = atoi(tmp.c_str());
            tmp = msg.substr(6);
            tmp = tmp.substr(0, tmp.size() - 1);
            file->AddOp(DELETE, pos, len, tmp, socket);
        }
        else if (buf[0] - '0' == CLOSE) {
            parent->myManager.oneLess(docId, socket);
            docId = -1;
        }
        else if (buf[0] - '0' == SYNC) {
            doc = parent->myManager.getContentFromId(docId);
            for (unsigned i = 0; i < doc.size(); i+=50) {
                std::string frag = doc.substr(i, 50);
                std::string temp = std::to_string(INSERT) + leftpad(i, 3) + leftpad(frag.size(), 3) + frag + "\n";
                write(socket, temp.c_str(), temp.size());
                printf("wrote %s on socket %d\n", temp.c_str(), socket);
            }
            write(socket, "00000000\n", 9);     // koniec wysyłania insertów
        }
    } while (c > 0);

    parent->terminateClient(this->socket);
}