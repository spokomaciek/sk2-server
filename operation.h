//
// Created by spokomaciek on 29.12.16.
//

#ifndef SK2_PROJEKT_OPERATION_H
#define SK2_PROJEKT_OPERATION_H

#include <string>
#include "command.h"

// w sumie bardziej struktura z konstruktorem
class Operation {
public:
    Operation(COMMAND command, int pos, int length, std::string ins, int from);
    COMMAND command;
    int pos;
    int length;
    std::string ins;
    int from;       // socket z któ©ego pochodzi
};


#endif //SK2_PROJEKT_OPERATION_H
