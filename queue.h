//
// Created by spokomaciek on 29.12.16.
//

#ifndef SK2_PROJEKT_QUEUE_H
#define SK2_PROJEKT_QUEUE_H


#include <vector>
#include "operation.h"
// kolejka z mutexem
class Queue {
private:
    std::vector<Operation> queue;
    pthread_mutex_t queue_mutex;
public:
    Queue();
    void insert(COMMAND command, int pos, int length, std::string ins, int from);
    unsigned long size();
    void popFront();
    Operation &operator[](int i);
};


#endif //SK2_PROJEKT_QUEUE_H
