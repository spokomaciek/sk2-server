//
// Created by spokomaciek on 29.12.16.
//

#include <sys/socket.h>
#include <cstring>
#include <netinet/in.h>
#include <cstdlib>
#include <unistd.h>
#include <cstdio>
#include <arpa/inet.h>
#include "server.h"

Server::Server() {
    this->socket = -1;
}
// inicjalizacja wszytskiego
bool Server::createServer(int port) {
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(struct sockaddr_in));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htons(INADDR_ANY);
    server_address.sin_port = htons(port);
    if ((this->socket = ::socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation"); return false;
    }
    printf("Created server socket: %d\n", this->socket);
    char on = 1;
    setsockopt(this->socket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

    if (bind(this->socket, (struct sockaddr*)&server_address, sizeof(struct sockaddr)) < 0) {
        perror("Binding IP"); return false;
    }
    printf("Bound IP\n");

    if (listen(this->socket, QUEUE_SIZE) < 0) {
        perror("Setting queue size"); return false;
    }

    return true;
}

Server::~Server() {
    if (this->socket != -1) {
        close(this->socket);
    }
}

// akceptowanie połączeń od klienta
void Server::play() {
    printf("Waiting for connections...\n");
    while (1) {
        int client_socket;
        sockaddr_in client_address;
        socklen_t len = sizeof(struct sockaddr);
        if ((client_socket = accept(this->socket, (struct sockaddr*)&client_address, &len)) < 0) {
            perror("Accepting a connection:");
        }
        this->handleConnection(client_socket, client_address);
    }

}

void *Server::asyncPlay(void *object) {
    reinterpret_cast<Server*>(object)->play();
    return 0;
}

// tworzenie nowego obiektu klienta i uruchomienie dla niego nowego wątku
void Server::handleConnection(int client_socket, sockaddr_in client_address) {
    printf("New client connected on socket %d\n", client_socket);
    std::string ip(inet_ntoa(client_address.sin_addr));
    printf("IP address is: %s\n", ip.c_str());
    printf("port is: %d\n", (int) ntohs(client_address.sin_port));

    Client *newClient = new Client(ip, (int) ntohs(client_address.sin_port), client_socket, this);
    pthread_t processId;
    pthread_create(&processId, NULL, Client::asyncPlay, (void*) newClient);
    clients.emplace(processId, newClient);
}

// sprzątanie po zamknięciu klienta
void Server::terminateClient(int socket) {
    pthread_t threadId = -1;
    for (auto &i : clients) {
        if (i.second->socket == socket) {
            threadId = i.first;
        }
    }
    myManager.oneLess(clients[threadId]->docId, socket);
    close(clients[threadId]->socket);
    delete clients[threadId];
    clients.erase(threadId);
    printf("Terminacja klienta. Pozostało: %lu\n", clients.size());
    pthread_exit(NULL);
}

// czyszczenie na końcu programu
void Server::KillEmAll() {
    for (auto &c : clients) {
        myManager.oneLess(c.second->docId, c.second->socket);
        close(c.second->socket);
        pthread_cancel(c.first);
    }
}
