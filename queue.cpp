//
// Created by spokomaciek on 29.12.16.
//

#include "queue.h"

void Queue::insert(COMMAND command, int pos, int length, std::string ins, int from) {
    pthread_mutex_lock(&queue_mutex);
    Operation newOp(command, pos, length, ins, from);
    queue.push_back(newOp);
    pthread_mutex_unlock(&queue_mutex);
}

Queue::Queue() {
    queue_mutex = PTHREAD_MUTEX_INITIALIZER;
}

unsigned long Queue::size() {
    return queue.size();
}

void Queue::popFront() {
    queue.erase(queue.begin());
}

Operation &Queue::operator[](int i) {
    return queue[i];
}
