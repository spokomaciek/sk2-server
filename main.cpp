#include <iostream>
#include <netinet/in.h>
#include <csignal>
#include "server.h"

pthread_mutex_t sleeper;

void interrupt_handler(int signum);

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <server_port>", argv[0]);
        return 1;
    }

    signal(SIGINT, interrupt_handler);

    sleeper = PTHREAD_MUTEX_INITIALIZER;

    Server *myServer = new Server();
    myServer->createServer(atoi(argv[1]));

    pthread_t player;
    pthread_mutex_lock(&sleeper);
    pthread_create(&player, NULL, Server::asyncPlay, (void*)myServer);
    pthread_mutex_lock(&sleeper);
    printf("\nInterrupted\n");

    pthread_cancel(player);
    myServer->KillEmAll();
    delete myServer;

    return 0;
}

void interrupt_handler(int signum) {
    pthread_mutex_unlock(&sleeper);

}