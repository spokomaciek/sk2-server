//
// Created by spokomaciek on 06.01.17.
//

#include "filemanager.h"
#include "command.h"

FileManager::FileManager() {
    availableFiles.push_back("a.txt");
    availableFiles.push_back("b.txt");
    availableFiles.push_back("c.txt");
    last_id = 1;
    adder = PTHREAD_MUTEX_INITIALIZER;
}

// wskaźnik na otwarty plik, albo utworzenie i wykonanie rekurencyjne
OpenFile *FileManager::getFile(std::string name) {
    for (unsigned i = 0; i < availableFiles.size(); ++i) {
        if (availableFiles[i] == name) {
            auto search = openFiles.find(name);
            if (search == openFiles.end()) {
                OpenFile *tmp = new OpenFile(name);
                openFiles.emplace(name, ++last_id);
                Files.emplace(last_id, tmp);
                pthread_t thrd;
                pthread_create(&thrd, NULL, OpenFile::asyncDo, tmp);
                return Files[last_id];
            }
            else {
                return Files[search->second];
            }
        }
    }
    FILE *fp = fopen(name.c_str(), "wb");  // create
    availableFiles.push_back(name);
    fclose(fp);
    return getFile(name);
}

int FileManager::getFileId(std::string name) {
    for (unsigned i = 0; i < availableFiles.size(); ++i) {
        if (availableFiles[i] == name) {
            auto search = openFiles.find(name);
            if (search == openFiles.end()) {
                OpenFile *tmp = new OpenFile(name);
                openFiles.emplace(name, ++last_id);
                Files.emplace(last_id, tmp);
                return last_id;
            }
            else {
                return search->second;
            }
        }
    }
    return 0;
}

std::string lleftpad(unsigned long n, unsigned len) {
    std::string temp = std::to_string(n);
    while (temp.size() < len) {
        temp = "0"+temp;
    }
    return temp;
}

std::string FileManager::getFileList() {
    std::string temp = "";
    for (unsigned i = 0; i < availableFiles.size(); ++i) {
        temp += availableFiles[i] + ",";
    }
    temp = std::to_string(FILELIST) + std::to_string(420) + lleftpad(temp.size() - 1, 3) + temp.substr(0, temp.size() - 1) + "\n";
    return temp;
}

std::string FileManager::getContentFromId(int id) {
    return Files[id]->content;
}

void FileManager::oneMore(int id, int socket) {
    pthread_mutex_lock(&adder);
    Files[id]->workers +=1;
    Files[id]->clients.push_back(socket);
    pthread_mutex_unlock(&adder);
}

void FileManager::oneLess(int id, int socket) {
    if (id == -1) return;
    pthread_mutex_lock(&adder);
    for (unsigned i = 0; i < Files[id]->clients.size(); ++i) {
        if (Files[id]->clients[i] == socket) {
            Files[id]->clients.erase(Files[id]->clients.begin() + i);
        }
    }
    Files[id]->workers -= 1;
    if (Files[id]->workers == 0) {
        Files[id]->Save();
        delete Files[id];
        Files.erase(id);
        for (auto &i : openFiles) {
            if (i.second == id) {
                openFiles.erase(i.first);
            }
        }
    }
    pthread_mutex_unlock(&adder);
}
