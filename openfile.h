//
// Created by spokomaciek on 03.01.17.
//

#ifndef SK2_PROJEKT_OPENFILE_H
#define SK2_PROJEKT_OPENFILE_H
#include <string>
#include "queue.h"

class OpenFile {
private:
    std::string filename;
    std::string content;
    pthread_mutex_t queue;
    std::vector<int> clients;
public:
    OpenFile(std::string fname);
    int workers;
    void Save();
    Queue opQueue;  // kolejka z operacjami do wykonania
    friend class FileManager;

    void AddOp(COMMAND command, int pos, int len, std::string ins, int from);

    void ExecuteOperations();

    // odpalenie asynchroniczne ExecuteOperations()
    static void* asyncDo(void *object);
};


#endif //SK2_PROJEKT_OPENFILE_H
