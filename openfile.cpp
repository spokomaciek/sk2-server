//
// Created by spokomaciek on 03.01.17.
//

#include <unistd.h>
#include "openfile.h"

OpenFile::OpenFile(std::string fname) {
    queue = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_lock(&queue);
    workers = 0;
    this->filename = fname;
    FILE *fp = fopen(filename.c_str(), "r");
    fseek(fp, 0, SEEK_END);
    this->content.resize(ftell(fp));
    rewind(fp);
    std::fread(&this->content[0], sizeof(char), this->content.size(), fp);
    fclose(fp);
}

void OpenFile::AddOp(COMMAND command, int pos, int len, std::string ins, int from) {
    opQueue.insert(command, pos, len, ins, from);
    pthread_mutex_unlock(&queue);
}

std::string leftpad(int n, unsigned len) {
    std::string temp = std::to_string(n);
    while (temp.size() < len) {
        temp = "0"+temp;
    }
    return temp;
}


// wykonywanie operacji w kolejce
void OpenFile::ExecuteOperations() {
    while (this) {
        pthread_mutex_lock(&queue);
        // propagacja zmiany na pozostałe operacje w kolejce
        for (unsigned i = 1; i < opQueue.size(); ++i) {
            if (opQueue[i].from == opQueue[0].from) continue;
            if (opQueue[i].command == INSERT) {
                if (opQueue[0].pos < opQueue[i].pos) {
                    opQueue[i].pos += opQueue[0].length;
                }
            } else {    // DELETE
                if (opQueue[0].pos < opQueue[i].pos) {
                    opQueue[i].pos -= opQueue[0].length;
                }
            }
        }
        // ograniczenie pozycji (zdarzało się wykraczanie)
        if (opQueue[0].pos > content.size()) {
            opQueue[0].pos = content.size() - 1;
            opQueue[0]. pos = opQueue[0].pos > 0 ? opQueue[0].pos : 0;
        }
        if (opQueue[0].command == INSERT) {
            content = content.substr(0, opQueue[0].pos) + opQueue[0].ins + content.substr(opQueue[0].pos);
        }
        else {
            content = content.substr(0, opQueue[0].pos - opQueue[0].length) + content.substr(opQueue[0].pos);
        }
        // wysłanie zmian do klientów z otwartym tym plikiem
        // propagacja -> jeżeli w kolejce jest już operacja od danego klienta w miejscu wcześniejszym to znaczy,
        // że trzeba dla niego przesunąć pozycję, bo on ma więcej znaków
        for (auto &c : clients) {
            int pos = opQueue[0].pos;
            if (opQueue[0].from == c) continue;
            for (unsigned i = 1; i < opQueue.size(); ++i) {
                if (opQueue[i].from == c) {
                    if (opQueue[i].pos < opQueue[0].pos) {
                        if (opQueue[i].command == INSERT) {
                            pos += opQueue[i].length;
                        }
                        else {
                            pos -= opQueue[i].length;
                        }
                    }
                }
            }
            std::string msg = std::to_string(opQueue[0].command) + leftpad(pos, 3) + leftpad(opQueue[0].length, 3) + opQueue[0].ins + "\n";
            write(c, msg.c_str(), msg.size());
            printf("wrote %s on socket %d\n", msg.c_str(), c);
        }
        opQueue.popFront();
        if (opQueue.size() != 0) {
            pthread_mutex_unlock(&queue);
        }
        else {
            for (auto &c : clients) {
                write(c, "00000000\n", 9);  // koniec synchronizacji
            }
        }
    }

}

void *OpenFile::asyncDo(void *object) {
    reinterpret_cast<OpenFile*>(object)->ExecuteOperations();
    return 0;
}

void OpenFile::Save() {
    FILE *fp = fopen(this->filename.c_str(), "wb");
    std::fwrite(&this->content[0], sizeof(char), this->content.size(), fp);
    fclose(fp);
}
